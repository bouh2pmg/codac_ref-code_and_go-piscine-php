<?php

function my_password_hash($password)
{
  $salt = mcrypt_create_iv(16, MCRYPT_DEV_URANDOM);
  return (["hash" => md5($salt.$password), "salt" => $salt]);
}

function my_password_verify($password, $salt, $hash)
{
  return (md5($salt.$password) == $hash);
}

?>
