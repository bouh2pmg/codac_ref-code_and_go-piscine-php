<?php

/*
** Transformation of several arrays to a map
*/

function my_create_map(...$arrays)
{
    $array_map = array ();

    foreach($arrays as $value)
        {
            if ($value == NULL || count($value) != 2)
                {
                    echo("The given arguments aren't good.\n");
                    return (NULL);
                }
            $array_map[$value[0]] = $value[1];
        }

    return ($array_map);
}

/*
$object1 = array("un", 1);
$object2 = array("deux", 2);
$object3 = array("trois", 3);

var_dump(my_create_map($object1, $object2, $object3));
*/

?>
