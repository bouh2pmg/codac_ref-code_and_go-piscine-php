<?php

class Gecko
{
    private $name;
    private $age;
    private $energy;
    private $drunk;

    public function __construct($n = null, $a = 0)
    {
        if ($n)
            echo "Hello $n !\n";
        else
            echo "Hello !\n";
        $this->name = $n;
        $this->age = $a;
        $this->energy = 100;
        $this->drunk = false;
    }

    public function __destruct()
    {
        if ($this->name)
            echo "Bye $this->name !\n";
        else
            echo "Bye !\n";
    }

    public function getName()
    {
        return $this->name;
    }

    public function setAge($a)
    {
        $this->age = $a;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function status()
    {
        switch ($this->age)
            {
            case 0:
                echo "Unborn Gecko\n";
                break;
            case 1:
            case 2:
                echo "Baby Gecko\n";
                break;
            case ($this->age <= 10 && $this->age > 2):
                echo "Adult Gecko\n";
                break;
            case 11:
            case 12:
            case 13:
                echo "Old Gecko\n";
                break;
            default:
                echo "Impossible Gecko\n";
                break;
            }
    }

    public function hello($string)
    {
        if ($this->drunk && rand(1, 6) == 1)
            {
                echo "I'm too drunk for that... hic !\n";
                return ;
            }
        if (is_string($string))
            {
                if ($this->name)
                    echo "Hello $string, I'm $this->name!\n";
                else
                    echo "Hello $string!\n";
            }
        else if (is_int($string) && $string >= 0)
            {
                for ($i = $string; $i > 0; --$i)
                    {
                        if ($this->name)
                            echo "Hello, I'm $this->name!\n";
                        else
                            echo "Hello !\n";
                    }
            }
    }

    public function getEnergy()
    {
        return $this->energy;
    }

    public function setEnergy($e)
    {
        if ($e <= 0)
            $this->energy = 0;
        else if ($e >= 100)
            $this->energy = 100;
        else
            $this->energy = $e;
    }

    public function eat($str)
    {
        $s = strtolower($str);

        if ($this->drunk && rand(1, 6) == 1)
            {
                echo "I'm too drunk for that... hic !\n";
                return ;
            }

        if ($s == "meat")
            {
                echo "Yummy!\n";
                $this->setEnergy($this->energy + 10);
            }
        else if ($s == "vegetable")
            {
                echo "Erk!\n";
                $this->setEnergy($this->energy - 10);
            }
        else
            echo "I can't eat this.\n";
    }

    public function work()
    {
        if ($this->drunk && rand(1, 6) == 1)
            {
                echo "I'm too drunk for that... hic !\n";
                return ;
            }
        if ($this->energy >= 25)
            {
                echo "I'm working T.T\n";
                $this->energy -= 9;
            }
        else
            {
                echo "Heyyy... I'm too sleepy, better take a nap!\n";
                $this->energy += 50;
                $this->drunk = false;
            }
    }

    public function fraternize($obj)
    {
        if ($this->drunk && rand(1, 6) == 1)
            {
                echo "I'm too drunk for that... hic !\n";
                return ;
            }
        if ($obj instanceof Gecko && $obj != $this)
            {
                if ($obj->getEnergy() >= 30 && $this->energy >= 30)
                    {
                        echo "I'm going to drink with " . $obj->getName() . " !\n";
                        echo "I'm going to drink with $this->name !\n";
                        $this->drunk = true;
                        $obj->setEnergy($obj->getEnergy() - 30);
                        $this->setEnergy($this->energy - 30);
                    }
                else if ($obj->getEnergy() >= 30)
                    {
                        echo "Sorry " . $obj->getName() . ", I'm too tired for going out tonight...\n";
                        echo "Oh ! That's too bad, another time then !\n";
                    }
                else if ($this->energy >= 30)
                    {
                        echo "Sorry $this->name, I'm too tired for going out tonight...\n";
                        echo "Oh ! That's too bad, another time then !\n";
                    }
                else
                    {
                        echo "Not today !\n";
                        echo "Not today !\n";
                    }
            }
        else if ($obj instanceof Snake)
            {
                if ($this->energy >= 10)
                    {
                        $this->energy = 0;
                        echo "LET'S RUN AWAY !!!\n";
                    }
                else
                    echo "...\n";
            }
        else
            echo "No way.\n";
    }
};