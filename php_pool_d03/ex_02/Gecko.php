<?php

class Gecko
{
    public $name;
    
    public function __construct($n = null)
    {
        if ($n)
            echo "Hello $n !\n";
        else
            echo "Hello !\n";
        $this->name = $n;
    }
};