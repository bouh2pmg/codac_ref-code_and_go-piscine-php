<?php 

namespace Imperium {
	class Soldier 
	{
		//ATTRIBUTS
		private $hp;
		private $attack;
		private $name;

		// CONSTRUCTEUR
		public function __construct($name, $hp = 50, $attack = 12) {
			$this->name = $name;
			$this->hp = $hp;
			$this->attack = $attack;
		}

		public function __toString() {
			return $this->name . " the " . __NAMESPACE__ . " Space Marine : " . $this->hp . " HP.";
		}

		// GETTERS
		public function getHp() {
			return $this->hp;
		}

		public function getAttack() {
			return $this->attack;
		}

		public function getName() {
			return $this->name;
		}

		//SETTERS
		public function setHp($new) {
			$this->hp = $new;
		}

		public function setAttack($new) {
			$this->attack = $new;
		}

		public function setName($new) {
			$this->name = $new;
		}

		//METHODS
		public function doDamage($soldier) {
			$soldier->setHp($soldier->getHp()-$this->attack);
		}
	} 
}

namespace Chaos {
	class Soldier 
	{
		//ATTRIBUTS
		private $hp;
		private $attack;
		private $name;

		// CONSTRUCTEUR
		public function __construct($name, $hp = 70, $attack = 12) {
			$this->name = $name;
			$this->hp = $hp;
			$this->attack = $attack;
		}

		public function __toString() {
			return $this->name . " the " . __NAMESPACE__ . " Space Marine : " . $this->hp . " HP.";
		}

		// GETTERS
		public function getHp() {
			return $this->hp;
		}

		public function getAttack() {
			return $this->attack;
		}

		public function getName() {
			return $this->name;
		}

		//SETTERS
		public function setHp($new) {
			$this->hp = $new;
		}

		public function setAttack($new) {
			$this->attack = $new;
		}

		public function setName($new) {
			$this->name = $new;
		}

		//METHODS
		public function doDamage($soldier) {
			$soldier->setHp($soldier->getHp()-$this->attack);
		}
	}
}
