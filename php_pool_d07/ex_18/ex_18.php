<?php

class Gecko
{
	private $_name;

	public function __construct($name) {
		$this->_name = $name;
	}

	public function getName() {
		return $this->_name;
	}

	public function correct($arcaniste) {
		// for each successful check of an instruction :
		// echo "Test <number> : Good!\n";
		// else
		// echo "Test <number> : KO.\n";
		$allclear = true;
		$arcaniste = new ReflectionClass($arcaniste);

		//TEST0
		if ($arcaniste->getName() == "Arcaniste" && $arcaniste->implementsInterface("IPerso"))
			echo "Test 0 : Good!\n";
		else {
			echo "Test 0 : KO.\n";
			$allclear = false;
		}

		//TEST1
		$aunit = $arcaniste->getParentClass();
		if ($aunit != NULL && $aunit->isAbstract() && $aunit->getName() == 'AUnit')
			echo "Test 1 : Good!\n";
		else {
			echo "Test 1 : KO.\n";
			$allclear = false;
		}

		if ($allclear == true)
			return (true);
		else
			return (false);
	}
}

