<?php 

class Character
{
	//ATTRIBUTS
	private $name;
	private $strength;
	private $magic;
	private $intelligence;
	private $life;
	protected static $i = 1;

	//CONSTRUCTEUR
	public function __construct($name = null) {
		if (!isset($name)) {
			$this->name = self::$i;
			self::$i++;
		}
		else 
			$this->name = $name;
		$this->strength = 0;
		$this->magic = 0;
		$this->intelligence = 0;
		$this->life = 100;
	}

	public function __toString() {
		if (is_integer($this->name))
			return "My name is " . self::CLASS . " " . $this->getName() . ".\n";
		return "My name is " . $this->getName() . ".\n";
	}

	protected function getName() {
		return $this->name;
	}

	protected function getStrength() {
		return $this->strength;
	}

	protected function getMagic() {
		return $this->magic;
	}

	protected function getIntelligence() {
		return $this->intelligence;
	}

	protected function getLife() {
		return $this->life;
	}
}

// foreach ([new Character, new Character("Julien"), new Character, new Character("mimi"), new Character("lulu"), new Character] as $character)
// {
// 	echo $character;
// }