<?php

include_once('Character.php');
include_once('Warrior.php');

class Mage extends Character {
	//ATTRIBUTS
	const CLASSE = "Mage";

	//CONSTRUCTEUR
	public function __construct($name) {
		$this->name = $name;
		parent::__construct($this->name);
		$this->life = 70;
		$this->agility = 10;
		$this->strength = 3;
		$this->wit = 10;

		echo $this->name . ": May the gods be with me.\n";
	}

	public function __destruct() {
		echo $this->name . ": By the four gods, I passed away...\n";
	}

	public function attack() {
		echo $this->name . ": Feel the power of my magic!\n";
	}
}

// $warrior = new Warrior("Jean-luc");
// $mage = new Mage("Robert");
// $warrior->attack();
// $mage->attack();

// echo $warrior->getName() . "\n";
// echo $warrior->getLife() . "\n";
// echo $warrior->getAgility() . "\n";
// echo $warrior->getStrength() . "\n";
// echo $warrior->getWit() . "\n";
// echo $warrior->getClasse() . "\n";
// echo $mage->getName() . "\n";
// echo $mage->getLife() . "\n";
// echo $mage->getAgility() . "\n";
// echo $mage->getStrength() . "\n";
// echo $mage->getWit() . "\n";
// echo $mage->getClasse() . "\n";

// $perso = new Warrior("Jean-luc");
// echo $perso->moveRight();
// echo $perso->moveLeft();
// echo $perso->moveUp();
// echo $perso->moveDown();