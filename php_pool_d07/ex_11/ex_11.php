<?php 

function my_autoload_function($class) {
    include $class . '.class.php';
}

spl_autoload_register('my_autoload_function');
