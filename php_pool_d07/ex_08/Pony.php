<?php 

class Pony 
{
	//ATTRIBUTS
	private $gender;
	private $name;
	private $color;

	//MAGIC METHODS
	public function __construct($gender, $name, $color) {
		$this->gender = $gender;
		$this->name = $name;
		$this->color = $color;
	}

	public function __toString() {
		return "Don't worry, I'm a pony!\n";
	}

	public function __get($name) {
		if ($name == 'gender' || $name == 'name' || $name == 'color') {
			echo "It's not right to get a private attribute!\n";
			return $this->$name;
		}
		else 
			echo "There is no attribute " . $name . ".\n";
	}

	public function __set($name, $value) {
		if ($name == 'gender' || $name == 'name' || $name == 'color') {
			echo "It's not right to set a private attribute!\n";
			$this->$name = $value;
		}
		else 
			echo "There is no attribute " . $name . ".\n";
	}

	public function __destruct() {
		echo "I'm a dead pony.\n";
	}

	public function __call($method, $args) {
		echo "I don't know what to do...\n";
	}

	//GETTERS
	// public function getGender() {
	// 	echo "It's not right to get a private attribute!\n";
	// 	return $this->gender;
	// }

	// public function getName() {
	// 	echo "It's not right to get a private attribute!\n";
	// 	return $this->name;
	// }

	// public function getColor() {
	// 	echo "It's not right to get a private attribute!\n";
	// 	return $this->color;
	// }

	// //SETTERS
	// public function setGender($new) {
	// 	echo "It's not right to set a private attribute!\n";
	// 	$this->gender = $new;
	// }

	// public function setName($new) {
	// 	echo "It's not right to set a private attribute!\n";
	// 	$this->name = $new;
	// }

	// public function setColor($new) {
	// 	echo "It's not right to set a private attribute!\n";
	// 	$this->color = $new;
	// }

	//METHODS
	public function speak() {
		echo "Hiii hiii hiii\n";
	}
}

// $pony = new Pony("female","lulu","blue");
// echo $pony->id;
// $pony->name = "momo";
// echo $pony->name;
// $pony->__set('name','blue');
// echo $pony->name;