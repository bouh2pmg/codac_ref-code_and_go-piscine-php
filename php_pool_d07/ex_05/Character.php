<?php
include_once('IMovable.php');

class Character implements IMovable {
	//ATTRIBUTS
	protected $name;
	protected $life;
	protected $agility;
	protected $strength;
	protected $wit;
	const CLASSE = '';

	//CONSTRUCTEUR
	public function __construct($name) {
		$this->name = $name;
		$this->life = 50;
		$this->agility = 2;
		$this->strength = 2;
		$this->wit = 2;
	}

	//GETTERS
	public function getName() {	return $this->name; }
	public function getLife() {	return $this->life; }
	public function getAgility() {	return $this->agility; }
	public function getStrength() {	return $this->strength; }
	public function getWit() {	return $this->wit; }
	public function getClasse() {  return static::CLASS; }

	//METHODS
	public function moveRight() {
		echo $this->name . ": moves right.\n";
	}
	public function moveLeft() {
		echo $this->name . ": moves left.\n";
	}
	public function moveUp() {
		echo $this->name . ": moves up.\n";
	}
	public function moveDown() {
		echo $this->name . ": moves down.\n";
	}
	final public function unsheathe() {
		echo $this->name . ": unsheathes his weapon.\n";
	}
}

