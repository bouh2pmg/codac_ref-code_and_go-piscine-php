<?php

class Dolly 
{
	public $age;
	public $animal;
	public $doctor;

	public function __construct($age, $animal, $doctor) {
		$this->age = $age;
		$this->animal = $animal;
		$this->doctor = $doctor;
	}

	public function __clone() {
		echo "I will survive !\n";
	}
}

function clone_object($object) {
	if (is_object($object)) {
		$newObject = clone $object;
		return $newObject;
	}
}

// $dol = new Dolly(12,"chat","noob");
// $var = clone_object($dol);
// var_dump($dol);
// var_dump($var);