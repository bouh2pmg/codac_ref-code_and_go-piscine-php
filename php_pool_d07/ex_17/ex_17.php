<?php 

abstract class Apothecary
{
	public function heal($object) {
		if ($object instanceOf Imperium)
			echo "No servant of the Emperor shall fall if I can help it.\n";
		else
			echo "The enemies of the Emperor shall be destroyed!\n";
	}
}

// class Imperium {}
// class SpaceMarine extends Imperium {}
// class Heretic {}

// $var = new Apothecary();
// var_dump($var);

// Apothecary::heal(new Imperium());
// Apothecary::heal(new SpaceMarine());
// Apothecary::heal(new Heretic());