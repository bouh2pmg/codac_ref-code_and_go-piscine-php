<?php

class Gecko
{
	private $_name;

	public function __construct($name) {
		$this->_name = $name;
	}

	public function correct($my_classes, $my_workspaces) {
		// for each successful check of an instruction :
		// echo "Test <number> : Good!\n";
		// else
		// echo "Test <number> : KO.\n";
		$allclear = true;

		$classes = array();
		foreach ($my_classes as $key => $class) {
			$new = new ReflectionClass($class);
			$classes[$key] = $new;
		}

		//TEST1
		foreach ($classes as $class) {
			if ($class->inNamespace() && in_array($class->getNamespaceName(), $my_workspaces)) {
				$bool = true;
				continue;
			}
			else {
				$bool = false;
				echo "Test 0 : KO.\n";
				$allclear = false;
				break;
			}
		}
		if ($bool == true)
			echo "Test 0 : Good!\n";

		//TEST2
		foreach ($classes as $class) {
			if (!($class->isCloneable()) && $class->isFinal() && $class->getInterfaces() == NULL && ($class->getParentClass() == null)) {
				$bool = true;
				continue;
			}
			else {
				$bool = false;
				echo "Test 1 : KO.\n";
				$allclear = false;
				break;
			}
		}
		if ($bool == true)
			echo "Test 1 : Good!\n";

		//TEST3 
		$prop = $classes[0]->getProperties();
		$propStat = $classes[0]->getStaticProperties();
		$methods = $classes[0]->getMethods();

		$newMethods = array();
		$newMethods1 = array();

		foreach ($methods as $key => $value) {
			$pos = strpos($value, '{');
			$value = substr_replace($value, '', $pos);
			$newMethods[$key] = $value;
		}

		foreach ($classes as $key => $class) {
			if ($key > 0) {

				$prop1 = $class->getProperties();
				$propStat1 = $class->getStaticProperties();
				$methods1 = $class->getMethods();

				foreach ($methods1 as $key => $value) {
					$pos = strpos($value, '{');
					$value = substr_replace($value, '', $pos);
					$newMethods1[$key] = $value;
				}

				if (count(array_diff(array_merge($prop, $prop1), array_intersect($prop, $prop1))) === 0 && count(array_diff(array_merge($newMethods, $newMethods1), array_intersect($newMethods, $newMethods1))) === 0  && count(array_diff(array_merge($propStat, $propStat1), array_intersect($propStat, $propStat1))) === 0) {
						$bool = true;
						continue;
				}
				else {
					$bool = false;
					echo "Test 2 : KO.\n";
					$allclear = false;
					break;
				}
			}
		}
		if ($bool == true)
			echo "Test 2 : Good!\n";

		if ($allclear == true)
			return (true);
		else
			return (false);
	}
}
