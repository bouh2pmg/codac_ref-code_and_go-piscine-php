<?php

class Gecko
{
	private $_name;

	public function __construct($name) {
		$this->_name = $name;
	}

	public function getName() {
		return $this->_name;
	}

	public function correct(Soldier $soldier) {
		// for each successful check of an instruction :
		// echo "Test <number> : Good!\n";
		// else
		// echo "Test <number> : KO.\n";
		$allclear = true;
		$bool = true;
		$sol = new ReflectionClass($soldier);

		//TEST0
		$attributs = array();
		$attributs = $sol->getProperties();
		$attributsPriv = $sol->getProperties(ReflectionProperty::IS_PRIVATE);

		foreach ($attributs as $attr) {
			if (!('name' == $attr->getName()) && !('attack' == $attr->getName()) && !('hp' == $attr->getName())) {
				$bool = false;
				break;
			}
		}

		if ($sol->getName() == "Soldier" && count(array_diff($attributs, $attributsPriv)) != 0) {
			$bool = false;
		}

		if ($bool == true) {
			echo "Test 0 : Good!\n";
		}
		else {
			echo "Test 0 : KO.\n";
			$allclear = false;
		}

		// PAS FINI :(

	}
}

