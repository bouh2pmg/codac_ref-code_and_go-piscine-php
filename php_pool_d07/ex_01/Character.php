<?php

class Character {
	//ATTRIBUTS
	protected $name;
	protected $life;
	protected $agility;
	protected $strength;
	protected $wit;
	const CLASSE = '';

	//CONSTRUCTEUR
	public function __construct($name) {
		$this->name = $name;
		$this->life = 50;
		$this->agility = 2;
		$this->strength = 2;
		$this->wit = 2;
	}

	//GETTERS
	public function getName() {	return $this->name; }
	public function getLife() {	return $this->life; }
	public function getAgility() {	return $this->agility; }
	public function getStrength() {	return $this->strength; }
	public function getWit() {	return $this->wit; }
	public function getClasse() {  return self::CLASS; }
}

// $perso = new Character("Jean-luc");
// echo $perso->getName();
// echo $perso->getLife();
// echo $perso->getAgility();
// echo $perso->getStrength();
// echo $perso->getWit();
// echo $perso->getClasse();