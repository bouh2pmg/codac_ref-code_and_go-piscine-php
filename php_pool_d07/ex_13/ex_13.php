<?php

// class Skill {}
// class Skill1 {}

// $var = new Skill();
// $var1 = new Skill();
// objects_comparison($var,$var1);
// objects_comparison($var1,$var1);
// objects_comparison($var,$var);

function objects_comparison($object1, $object2) {
	if ($object1 === $object2)
		// objects refer to the same instance of class
		// ex : $var(3, 3) and $var(3, 3)
		echo "Objects are the same.\n";
	else if ($object1 == $object2)
		// ex : $var(3, 3) and $othervar(3, 3)
		echo "Objects are equal.\n";
	else
		echo "\n";
}