<?php

class Animal
{
    const MAMMAL = 0;
    const FISH = 1;
    const BIRD = 2;

    private $name;
    private $legs;
    private $type;
    
    public function __construct($name, $legs, $type)
    {
        $this->name = $name;
        $this->legs = $legs;
        $this->type = $type;

        echo "My name is " . $name . " and I am a " . $this->getType() . " !\n";
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLegs()
    {
        return $this->legs;
    }

    public function getType()
    {
        if ($this->type == Animal::MAMMAL)
            return "mammal";
        else if ($this->type == ANIMAL::FISH)
            return "fish";
        else if ($this->type == ANIMAL::BIRD)
            return "bird";
        return "";
    }
}
