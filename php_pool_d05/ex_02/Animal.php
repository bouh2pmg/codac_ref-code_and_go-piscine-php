<?php

class Animal
{
    const MAMMAL = 0;
    const FISH = 1;
    const BIRD = 2;

    private static $mammals = 0;
    private static $fish = 0;
    private static $birds = 0;
    
    protected $name;
    protected $legs;
    protected $type;
    
    public function __construct($name, $legs, $type)
    {
        $this->name = $name;
        $this->legs = $legs;
        $this->type = $type;

        if ($type == Animal::MAMMAL)
            ++Animal::$mammals;
        else if ($type == Animal::FISH)
            ++Animal::$fish;
        else if ($type == Animal::BIRD)
            ++Animal::$birds;

        echo "My name is " . $name . " and I am a " . $this->getType() . " !\n";
    }

    public function __destruct()
    {
        if ($this->type == Animal::MAMMAL)
            --Animal::$mammals;
        else if ($this->type == Animal::FISH)
            --Animal::$fish;
        else if ($this->type == Animal::BIRD)
            --Animal::$birds;
    }
    
    public function getName()
    {
        return $this->name;
    }

    public function getLegs()
    {
        return $this->legs;
    }

    public function getType()
    {
        if ($this->type == Animal::MAMMAL)
            return "mammal";
        else if ($this->type == ANIMAL::FISH)
            return "fish";
        else if ($this->type == ANIMAL::BIRD)
            return "bird";
        return "";
    }

    static public function getNumberOfAnimalsAlive()
    {
        $count = Animal::$mammals + Animal::$fish + Animal::$birds;
        echo "There " . (($count > 1) ? ("are") : ("is")) .  " currently " . $count . " animal" . (($count > 1) ? ("s") : ("")) . " alive in our world.\n";
        return $count;
    }

    static public function getNumberOfMammals()
    {
        echo "There " . ((Animal::$mammals > 1) ? ("are") : ("is")) . " currently " . Animal::$mammals . " mammal" . ((Animal::$mammals > 1) ? ("s") : ("")) . " alive in our world.\n";
        return Animal::$mammals;
    }
    
    static public function getNumberOfFishes()
    {
        echo "There " . ((Animal::$fish > 1) ? ("are") : ("is")) . " currently " . Animal::$fish . " fish" . ((Animal::$fish > 1) ? ("es") : ("")) . " alive in our world.\n";
        return Animal::$fish;
    }
    
    static public function getNumberOfBirds()
    {
        echo "There " . ((Animal::$birds > 1) ? ("are") : ("is")) . " currently " . Animal::$birds . " bird" . ((Animal::$birds > 1) ? ("s") : ("")) . " alive in our world.\n";
        return Animal::$birds;
    }
}