<?php

include_once("Animal.php");

class Shark extends Animal
{
    protected $frenzy;

    public function __construct($name)
    {
        parent::__construct($name, 0, Animal::FISH);
        echo "A KILLER IS BORN !\n";
        $this->frenzy = false;
    }

    public function smellBlood($trigger)
    {
        if (is_bool($trigger))
            $this->frenzy = $trigger;
    }

    public function status()
    {
        if ($this->frenzy === true)
            echo $this->name . " is smelling blood and wants to kill.\n";
        else
            echo $this->name . " is swimming peacefully.\n";
    }

    public function eat($food)
    {
        if ($food instanceof Animal && $food != $this)
            {
                echo $this->name . " ate a " . $food->getType() . " named " . $food->getName() . ".\n";
                $this->frenzy = false;
            }
        else
            echo $this->name . ": It's not worth my time.\n";
    }
}