<?php
include_once("db_pdo.php");

const ERROR_LOG_FILE = "errors.log";

const DB_HOST     = "localhost";
const DB_USERNAME = "root";
const DB_PASSWORD = "root";
const DB_PORT = 3306;
const DB_NAME = "gecko";


if(count($argv) != 5)
{
	$err_msg = "Bad params! Usage: php add_user.php login password password_conf role\n";
	echo $err_msg;
	file_put_contents(ERROR_LOG_FILE, $err_msg, FILE_APPEND);
	die();
}

$user_login         = $argv[1];
$user_password      = $argv[2];
$user_password_conf = $argv[3];
$user_role          = strtoupper($argv[4]);

if($user_password != $user_password_conf)
{
	$err_msg = "Passwords don't match\n";
	echo $err_msg;
	file_put_contents(ERROR_LOG_FILE, $err_msg, FILE_APPEND);
	die();
}
$user_password = hash("sha256", $user_password);

if(!in_array($user_role, array("ADM", "GLOBAL", "INVITE")))
{
	$err_msg = "Bad role: ADM|GLOBAL|INVITE\n";
	echo $err_msg;
	file_put_contents(ERROR_LOG_FILE, $err_msg, FILE_APPEND);
	die();
}

$dbh = connect_db(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_PORT, DB_NAME);
if($dbh === false)
{
	$err_msg = "Error connection to DB\n";
	echo $err_msg;
	file_put_contents(ERROR_LOG_FILE, $err_msg, FILE_APPEND);
	die();	
}
else
	echo "Connection to DB successful\n";


$sql_query = "INSERT INTO users (name,password,created_at,role) VALUES (".$dbh->quote($user_login).",".$dbh->quote($user_password).",CURDATE(),".$dbh->quote($user_role).");";
$result    = $dbh->query($sql_query);
if($result->rowCount() != 1)
{
	$err_msg = "Error MySQL, user not added, more infos in ".ERROR_LOG_FILE."\n";
	echo $err_msg;
	file_put_contents(ERROR_LOG_FILE, $err_msg, FILE_APPEND);
	die();		
}
else
	echo "User added to DB\n";
?>