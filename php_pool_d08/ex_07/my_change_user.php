<?php
include_once("db_pdo.php");


function my_change_user(PDO $bdd, ...$names) {
	echo "Good luck with the user DB!\n";
	$names_changed = array();

	foreach ($names as $one_name) {
		$user_name = ucfirst($one_name)."42";
		$sql_query = "UPDATE users SET name=".$bdd->quote($user_name)." WHERE name=".$bdd->quote($one_name)." LIMIT 1;";

		$result = $bdd->query($sql_query);
		if($result->rowCount() != 1)
			throw new Exception("User not found");		
		else
			array_push($names_changed, $user_name);
	}

	sort($names_changed);
	return $names_changed;
}


$dbh = connect_db("localhost", "root", "root", "3306", "gecko");
if($dbh !== false)
{
	$results = my_change_user($dbh, "Hugo4242", "Loic4242");
	var_dump($results);
}
?>