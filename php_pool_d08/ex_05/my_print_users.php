<?php
include_once("db_pdo.php");

function my_print_users(PDO $dbh, ...$ids) {
	if(count($ids) == 0)
		throw new Exception("Invalid id given");

	$sql_query = "SELECT id,name FROM users WHERE id in (";
	foreach ($ids as $id) {
		if(is_int($id))
		{
			$sql_query .= $id.",";
		}
		else
			throw new Exception("Invalid id given");
	}
	$sql_query = substr($sql_query, 0, -1);
	$sql_query .= ");";

	$results_obj = $dbh->query($sql_query);
	$results_arr = $results_obj->fetchAll(PDO::FETCH_ASSOC);
	if(count($results_arr) == 0)
		return false;

	foreach ($results_arr as $row)
		echo $row["id"]."=>".$row["name"]."\n";

	return true;
}


$dbh = connect_db("localhost", "root", "root", "3306", "gecko");
if($dbh !== false)
{
	$ret = my_print_users($dbh,1,2,3,4);
	if($ret)
		echo "T\n";
	else
		echo "F\n";
}

?>