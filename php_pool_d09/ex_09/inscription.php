<!DOCTYPE html>
<HTML>
	<head>
	</head>
	<body>
		<?php

		const ERROR_LOG_FILE = "errors.log";
		const DB_HOST = "localhost";
		const DB_USERNAME = "root";
		const DB_PASSWORD = "";
		const DB_PORT = "3306";
		const DB_NAME = "gecko";

		$create = true; // passe à FALSE si l'un des champs est mal rempli

		if (isset($_POST['name']) && (strlen($_POST['name']) < 3 || strlen($_POST['name']) > 10)) {
			echo "Invalid name <br/>";
			$create = false;
		}
		if (isset($_POST['email']) && (!preg_match('#^[\w.-]+@[\w.-]+\.[a-z]{2,6}$#i', $_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))) {
			echo "Invalid email <br/>";
			$create = false;
		}
		if (isset($_POST['password']) && isset($_POST['password_confirmation'])) {
			if ((strlen($_POST['password']) >= 3 && strlen($_POST['password']) <= 10) && ($_POST['password'] == $_POST['password_confirmation']))
				$password = password_hash($_POST['password'], PASSWORD_DEFAULT);
			else {
				echo "Invalid password or password confirmation <br/>";
				$create = false;
			}
		}

		if (isset($_POST['name']) && $_POST['password'] && $_POST['email'] && $create == true) {
			// CONNEXION A LA BDD
		    try { 
		        $bdd = new PDO("mysql:host=".DB_HOST.";port=".DB_PORT.";dbname=".DB_NAME, DB_USERNAME, DB_PASSWORD);
				$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    } 
		    catch (PDOException $e) {
		        $error = $e->getMessage();
		        $dt  = new DateTime();
			    $txt = $dt->format('Y-m-d H:i:s') . " : " . $error . "\n" ;
		        $filepath = getcwd() . "/" . ERROR_LOG_FILE;
		        file_put_contents($filepath, $txt, FILE_APPEND);
		    }

		    try {
				$requete = $bdd->prepare('INSERT INTO users(name, password, email, is_admin, created_at) VALUES (:name,:password,:email,:is_admin,:created_at)');
			   	$requete->execute(array(
					"name" => $_POST['name'], 
					"password" => $password,
					"is_admin" => '0',
					"email" => $_POST['email'],
					"created_at" => date("y-m-d")
					));
			   	echo "User created\n";
			}
			catch (PDOException $e) {
				$error = $e->getMessage();
			    $dt  = new DateTime();
			    $filepath = getcwd() . "/" . ERROR_LOG_FILE;
			    $txt = $dt->format('Y-m-d H:i:s') . " : " . $error . "\n" ;
			    file_put_contents($filepath, $txt, FILE_APPEND);
			}
		}

		?>

		<form method="post" action="inscription.php">

			<label for="name">Name</label> <br />
			<input type="text" name="name" id="name" value="name" min="3" max="10" required /><br />

			<label for="email">Email</label> <br />
			<input type="text" name="email" id="email" value="email" required /><br />

			<label for="password">Password</label> <br />
			<input type="password" name="password" id="password" value="password" min="3" max="10" required /><br />

			<label for="password_confirmation">Password confirmation</label> <br />
			<input type="password" name="password_confirmation" id="password_confirmation" value="password_confirmation" min="3" max="10" required /><br />

			<input type="submit" value="Submit" /> 
		</form>

	</body>
</HTML>