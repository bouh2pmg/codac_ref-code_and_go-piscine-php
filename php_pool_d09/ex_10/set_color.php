<!DOCTYPE html>
<HTML>
	<head>
	</head>
	<body>

	<?php 
		if (isset($_POST['name'])) {
			$colors = array("white", "black", "red", "blue");
			if (in_array($_POST['name'], $colors)) {
				setcookie('background_color', $_POST['name'], time() + 365*24*3600, null, null, false, true);
				header('Location: show_color.php', true, 302);
				exit;
			}
			else {
				echo "Invalid color <br />";
				if (isset($_COOKIE['background_color'])) {
					unset($_COOKIE['background_color']);
					setcookie('background_color', '', time() - 3600);
				}
			}
		}
	?>

		<form method="post" action="#">
			<input type="text" name="name" id="name" value="background" required /><br />
			<input type="submit" value="Submit" /> 
		</form>

	</body>
</HTML>