<?php 

$defaultName = 'platypus';

if (isset($_GET['name'])) {
	echo "Hello " . $_GET['name'];
	setcookie('name', $_GET['name'], time() + 365*24*3600, null, null, false, true); 
}
else if (isset($_COOKIE['name']))
	echo "Hello " . $_COOKIE['name'];
else
	echo "Hello " . $defaultName;