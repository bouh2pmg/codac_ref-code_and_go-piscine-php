<?php
session_start(); 

$defaultName = 'platypus';

if (isset($_GET['name'])) {
	echo "Hello " . $_GET['name'];
	$_SESSION['name'] = $_GET['name'];
}
else if (isset($_SESSION['name'])) {
	echo "Hello " . $_SESSION['name'];
}
else
	echo "Hello " . $defaultName;
