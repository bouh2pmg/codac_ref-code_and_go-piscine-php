<?php

function my_swap(&$ptr1, &$ptr2) {
    $tmp = $ptr1;
    $ptr1 = $ptr2;
    $ptr2 = $tmp;
}