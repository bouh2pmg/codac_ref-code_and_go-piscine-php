<?php

include_once("IUnit.php");

abstract class AMonster implements IUnit
{
    protected $name;
    protected $hp;
    protected $ap;

    protected $damage;
    protected $apcost;

    protected $closeTo;
    
    public function __construct($name)
    {
        $this->name = $name;
        $this->hp = 0;
        $this->ap = 0;
        $this->damage = 0;
        $this->apcost = 0;
        $this->closeTo = null;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getHp()
    {
        return $this->hp;
    }

    public function getAp()
    {
        return $this->ap;
    }

    public function getDamage()
    {
        return $this->damage;
    }

    public function getApcost()
    {
        return $this->apcost;
    }

    public function getClose()
    {
        return $this->closeTo;
    }
    
    public function equip($weapon)
    {
        if ($this->hp <= 0)
            return false;
        echo "Monsters are proud and fight with their own bodies.\n";
    }

    # yes i know instance of is not perfect, but it will do the job here
    public function attack($target)
    {
        if ($this->hp <= 0)
            return false;
        if (!($target instanceOf IUnit))
            throw new Exception("Error in AMonster. Parameter is not an IUnit.");
        if ($this == $target)
            return ;
        if ($this->closeTo != $target)
            {
                echo $this->name . ": I'm too far away from " . $target->getName() . ".\n";
                return false;
            }
        if ($this->ap >= $this->apcost)
            {
                echo $this->name . " attacks " . $target->getName() . ".\n";
                $this->ap -= $this->apcost;
                $target->receiveDamage($this->damage);
            }
    }

    public function receiveDamage($dmg)
    {
        if ($this->hp <= 0)
            return false;
        $this->hp -= $dmg;
    }

    public function moveCloseTo($target)
    {
        if ($this->hp <= 0)
            return false;
        if (!($target instanceOf IUnit))
            throw new Exception("Error in AMonster. Parameter is not an IUnit.");
        if ($target == $this)
            return ;
        if ($target != $this->closeTo)
            {
                $this->closeTo = $target;
                echo $this->name . " is moving closer to " . $target->getName() . ".\n";
            }
    }

    public function recoverAP()
    {
        if ($this->hp <= 0)
            return false;
        $this->ap += 7;
        if ($this->ap > 50)
            $this->ap = 50;
    }
}