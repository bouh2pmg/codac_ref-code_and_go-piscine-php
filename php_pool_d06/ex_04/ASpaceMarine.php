<?php

include_once("IUnit.php");

abstract class ASpaceMarine implements IUnit
{
    protected $name;
    protected $hp;
    protected $ap;
    protected $weapon;

    protected $closeTo;
    
    public function __construct($name)
    {
        $this->name = $name;
        $this->hp = 0;
        $this->ap = 0;
        $this->weapon = null;
        $this->closeTo = null;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getHp()
    {
        return $this->hp;
    }

    public function getAp()
    {
        return $this->ap;
    }

    public function getWeapon()
    {
        return $this->weapon;
    }

    public function equip($weapon)
    {
    }

    public function attack($target)
    {
    }

    public function receiveDamage($dmg)
    {
    }

    public function moveCloseTo($target)
    {
    }

    public function recoverAP()
    {
    }
}