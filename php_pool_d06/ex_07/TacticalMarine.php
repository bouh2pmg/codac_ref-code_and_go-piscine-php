<?php

include_once("ASpaceMarine.php");
include_once("PlasmaRifle.php");

class TacticalMarine extends ASpaceMarine
{
    public function __construct($name)
    {
        parent::__construct($name);
        echo $name . " on duty.\n";
        $this->hp = 100;
        $this->equip(new PlasmaRifle());
        $this->ap = 20;
    }

    public function __destruct()
    {
        echo $this->name . " the Tactical Marine has fallen !\n";
    }

    public function recoverAp()
    {
        if ($this->hp <= 0)
            return false;
        $this->ap += 12;
        if ($this->ap > 50)
            $this->ap = 50;
    }
}