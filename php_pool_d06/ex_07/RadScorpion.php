<?php

include_once("AMonster.php");
include_once("AssaultTerminator.php");

class RadScorpion extends AMonster
{
    static private $id = 1;
    public function __construct()
    {
        parent::__construct("RadScorpion #" . RadScorpion::$id++);
        $this->hp = 80;
        $this->ap = 50;
        $this->damage = 25;
        $this->apcost = 8;
        echo $this->name . ": Crrr !\n";
    }

    public function __destruct()
    {
        echo $this->name . ": SPROTCH !\n";
    }

    public function attack($target)
    {
        if ($this->hp <= 0)
            return false;
        if (!($target instanceOf IUnit))
            throw new Exception("Error in AMonster. Parameter is not an IUnit.");
        if ($this == $target)
            return ;
        if ($this->closeTo != $target)
            {
                echo $this->name . ": I'm too far away from " . $target->getName() . ".\n";
                return false;
            }
        if ($this->ap >= $this->apcost)
            {
                echo $this->name . " attacks " . $target->getName() . ".\n";
                $this->ap -= $this->apcost;
                if (!($target instanceof AssaultTerminator))
                    $target->receiveDamage($this->damage * 2);
                else
                    $target->receiveDamage($this->damage);
            }
    }
        
}