<?php

include_once("IUnit.php");
include_once("AWeapon.php");

abstract class ASpaceMarine implements IUnit
{
    protected $name;
    protected $hp;
    protected $ap;
    protected $weapon;

    protected $closeTo;
    
    public function __construct($name)
    {
        $this->name = $name;
        $this->hp = 0;
        $this->ap = 0;
        $this->weapon = null;
        $this->closeTo = null;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getHp()
    {
        return $this->hp;
    }

    public function getAp()
    {
        return $this->ap;
    }

    public function getWeapon()
    {
        return $this->weapon;
    }

    public function equip($weapon)
    {
        if ($this->hp <= 0)
            return false;
        if (!($weapon instanceof AWeapon))
            throw new Exception("Error in ASpaceMarine. Parameter is not an AWeapon.");
        if ($weapon->getHolder() == null)
            {
                if ($this->weapon)
                    $this->weapon->setHolder(null);
                $this->weapon = $weapon;
                echo $this->name . " has been equipped with a " . $weapon->getName() . ".\n";
                $weapon->setHolder($this);
            }
    }

    public function attack($target)
    {
        if ($this->hp <= 0)
            return false;
        if (!($target instanceof IUnit))
            throw new Exception("Error in ASpaceMarine. Parameter is not an IUnit.");
        if (!$this->weapon)
            {
                echo $this->name . ": Hey, this is crazy. I'm not going to fight this empty handed.\n";
                return ;
            }
        if ($this->weapon->isMelee() && $this->closeTo != $target)
            {
                echo $this->name . ": I'm too far away from " . $target->getName() . ".\n";
                return ;
            }
        if ($this->ap >= $this->weapon->getApcost())
            {
                $this->ap -= $this->weapon->getApcost();
                echo $this->name . " attacks " . $target->getName() . " with a " . $this->weapon->getName() . ".\n";
                $target->receiveDamage($this->weapon->getDamage());
                $this->weapon->attack();
            }
            
    }

    public function receiveDamage($dmg)
    {
        if ($this->hp <= 0)
            return false;
        $this->hp -= $dmg;
    }

    public function moveCloseTo($target)
    {
        if ($this->hp <= 0)
            return false;
        if (!($target instanceOf IUnit))
            throw new Exception("Error in ASpaceMarine. Parameter is not an IUnit.");
        if ($target == $this)
            return ;
        if ($target != $this->closeTo)
            {
                $this->closeTo = $target;
                echo $this->name . " is moving closer to " . $target->getName() . ".\n";
            }
      }

    public function recoverAP()
    {
        if ($this->hp <= 0)
            return false;
        $this->ap += 9;
        if ($this->ap > 50)
            $this->ap = 50;
    }
}