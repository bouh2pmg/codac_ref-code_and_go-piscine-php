<?php

include_once("AMonster.php");
include_once("ASpaceMarine.php");

class SpaceArena
{
    private $monsters;
    private $spaceMarines;
    
    public function __construct()
    {
        $this->monsters = Array();
        $this->spaceMarines = Array();
    }

    public function __destruct()
    {
    }

    public function enlistMonsters($mons)
    {
        foreach ($mons as $m)
            {
                if (!($m instanceof AMonster))
                    throw new Exception("Stop trying to cheat !\n");
                if (!(in_array($m, $this->monsters)))
                    $this->monsters[] = $m;
            }
    }

    public function enlistSpaceMarines($spaceM)
    {
        foreach ($spaceM as $sp)
            {
                if (!($sp instanceof ASpaceMarine))
                    throw new Exception("Stop trying to cheat !\n");
                if (!(in_array($sp, $this->spaceMarines)))
                    $this->spaceMarines[] = $sp;
            }        
    }

    public function fight()
    {
        $ret = true;
        if (count($this->monsters) == 0)
            {
                echo "No monster available to fight.\n";
                $ret =  false;
            }
        if (count($this->spaceMarines) == 0)
            {
                echo "Those cowards ran away.\n";
                $ret = false;
            }
        if (!$ret)
            return $ret;

        $i = 0;
        $j = 0;
        $oldM = null;
        $oldSp = null;
        $originalCountM = count($this->monsters);
        $originalCountSp = count($this->spaceMarines);
        while ($i < $originalCountM && $j < $originalCountSp)
            {
                $m = $this->monsters[$i];
                $sp = $this->spaceMarines[$j];
                if ($sp != $oldSp)
                    {
                        $oldSp = $sp;
                        echo $sp->getName() . " has entered the arena.\n";
                    }
                if ($m != $oldM)
                    {
                        $oldM = $m;
                        echo $m->getName() . " has entered the arena.\n";
                    }
                while ($sp->getHp() > 0 && $m->getHp() > 0)
                    {
                        $bap = $sp->getAp();
                        $sp->attack($m);
                        if ($bap == $sp->getAp())
                            {
                                if ($bap < $sp->getWeapon()->getApcost() && (!$sp->getWeapon()->isMelee() || ($sp->getWeapon()->isMelee() && $sp->getClose() == $m)))
                                    $sp->recoverAP();
                                else
                                    $sp->moveCloseTo($m);
                            }
                        $bap = $m->getAp();
                        $m->attack($sp);
                        if ($bap == $m->getAp())
                            {
                                if ($bap < $m->getApcost() && $m->getClose() == $sp)
                                    $m->recoverAP();
                                else
                                    $m->moveCloseTo($sp);
                            }
                    }
                if ($sp->getHp() <= 0)
                    {                        
                        unset($this->spaceMarines[$j]);
                        ++$j;
                        $m->recoverAp();
                    }
                else
                    {
                        unset($this->monsters[$i]);
                        ++$i;
                        $sp->recoverAp();
                    }
            }
        $this->spaceMarines = array_values($this->spaceMarines);
        $this->monsters = array_values($this->monsters);
        if (count($this->spaceMarines) > count($this->monsters))
            echo "The spaceMarines are victorious.\n";
        else
            echo "The monsters are victorious.\n";
    }
}