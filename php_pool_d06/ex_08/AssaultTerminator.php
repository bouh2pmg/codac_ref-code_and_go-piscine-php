<?php

include_once("ASpaceMarine.php");
include_once("PowerFist.php");

class AssaultTerminator extends ASpaceMarine
{
    public function __construct($name)
    {
        parent::__construct($name);
        echo $name . " has teleported from space.\n";
        $this->hp = 150;
        $this->ap = 30;
        $this->equip(new PowerFist());
    }

    public function __destruct()
    {
        echo "BOUUUMMMM ! " . $this->name . " has exploded.\n";
    }

    public function receiveDamage($dmg)
    {
        if ($this->hp <= 0)
            return false;
        $dmg -= 3;
        if ($dmg < 1)
            $dmg = 1;
        $this->hp -= $dmg;
    }
}